package main

import "fmt"
import "os"
import "strconv"

// Move
type Move struct {
	From int
	To   int
}

type Spot bool

type Board [15]Spot

type Moves []Move

func main() {

	empty, err := strconv.Atoi(os.Args[1])
	if err != nil {
		// handle error
		fmt.Println(err)
		os.Exit(2)
	}
	board := Board{
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
	}
	board[empty-1] = true
	var moves []Move
	solve(board, moves)
}

func (spot Spot) String() string {
	if spot {
		return "⚪"
	}
	return "⚫"
}

func (b *Board) printBoard() {
	fmt.Printf("          %s\n", b[0])
	fmt.Printf("        %s   %s\n", b[1], b[2])
	fmt.Printf("      %s   %s   %s\n", b[3], b[4], b[5])
	fmt.Printf("    %s   %s   %s   %s\n", b[6], b[7], b[8], b[9])
	fmt.Printf("  %s   %s   %s   %s   %s\n", b[10], b[11], b[12], b[13], b[14])
	fmt.Println()
	fmt.Println()
	fmt.Println()
}

func (move Move) String() string {
	return fmt.Sprintf("{%d to %d}", move.From+1, move.To+1)
}

func (moves Moves) printMoves() {
	fmt.Printf("[ ")
	for _, m := range moves {
		fmt.Printf("%s ", m)
	}
	fmt.Printf("]\n")

}

func solve(board Board, moves Moves) bool {
	if len(moves) > 0 {
		var lm = moves[len(moves)-1]
		board[(lm.From+lm.To)/2] = true
		board[lm.From] = true
		board[lm.To] = false
		//did I win?
		if board.didIWin() {
			moves.printMoves()
			board.printBoard()

			return true
		}
	}
	//printBoard(board)
	//printMoves(moves)
	for i, h := range board {
		if h {
			pm := getPotentialMoves(i)
			for _, m := range pm {
				if !board[m] && !board[(m+i)/2] {
					var nm = Move{m, i}
					var moves2 = append(moves, nm)
					ret := solve(board, moves2)
					if ret {
						fmt.Println(nm)
						board.printBoard()
						return true //otherwise it will show every possible winning scenario
					}
				}
			}
		}
	}
	return false
}

func (b *Board) didIWin() bool {
	var total int
	for _, h := range b {
		if !h {
			total++
		}
	}
	return total < 2
}

func getPotentialMoves(pos int) []int {
	moves := [15][]int{
		[]int{3, 5},
		[]int{6, 8},
		[]int{7, 9},
		[]int{5, 10, 12, 0},
		[]int{11, 13},
		[]int{0, 3, 12, 14},
		[]int{1, 8},
		[]int{2, 9},
		[]int{6, 1},
		[]int{2, 7},
		[]int{3, 12},
		[]int{13, 4},
		[]int{10, 3, 14, 5},
		[]int{11, 4},
		[]int{12, 5},
	}
	return moves[pos]
}
